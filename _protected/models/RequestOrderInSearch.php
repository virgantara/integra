<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RequestOrderIn;

/**
 * RequestOrderInSearch represents the model behind the search form of `app\models\RequestOrderIn`.
 */
class RequestOrderInSearch extends RequestOrderIn
{

    public $namaSender;
    public $noRo;
<<<<<<< HEAD
=======
    public $tanggalPengajuan;
    public $tanggalPenyetujuan;
>>>>>>> ea7b4f3fd30ed6d68a4074146bced5dd29909ea0

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'perusahaan_id', 'departemen_id', 'ro_id'], 'integer'],
<<<<<<< HEAD
            [['created','namaSender','noRo'], 'safe'],
=======
            [['created','namaSender','noRo','tanggalPengajuan','tanggalPenyetujuan'], 'safe'],
>>>>>>> ea7b4f3fd30ed6d68a4074146bced5dd29909ea0
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestOrderIn::find();

        $query->where([self::tableName().'.departemen_id'=>Yii::$app->user->identity->departemen]);

        $query->joinWith(['ro as ro','ro.departemen as d']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
<<<<<<< HEAD
        ]);

         $dataProvider->sort->attributes['namaSender'] = [
=======
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['namaSender'] = [
>>>>>>> ea7b4f3fd30ed6d68a4074146bced5dd29909ea0
            'asc' => ['nama'=>SORT_ASC],
            'desc' => ['nama'=>SORT_DESC]
        ];

<<<<<<< HEAD
         $dataProvider->sort->attributes['noRo'] = [
=======
        $dataProvider->sort->attributes['noRo'] = [
>>>>>>> ea7b4f3fd30ed6d68a4074146bced5dd29909ea0
            'asc' => ['no_ro'=>SORT_ASC],
            'desc' => ['no_ro'=>SORT_DESC]
        ];

<<<<<<< HEAD
=======
        $dataProvider->sort->attributes['tanggalPengajuan'] = [
            'asc' => ['ro.tanggal_pengajuan'=>SORT_ASC],
            'desc' => ['ro.tanggal_pengajuan'=>SORT_DESC]
        ];

        $dataProvider->sort->attributes['tanggalPenyetujuan'] = [
            'asc' => ['ro.tanggal_penyetujuan'=>SORT_ASC],
            'desc' => ['ro.tanggal_penyetujuan'=>SORT_DESC]
        ];

>>>>>>> ea7b4f3fd30ed6d68a4074146bced5dd29909ea0
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'perusahaan_id' => $this->perusahaan_id,
            'departemen_id' => $this->departemen_id,
            'ro_id' => $this->ro_id,
            'created' => $this->created,
        ]);

        $query->andFilterWhere(['like', 'd.nama', $this->namaSender]);
<<<<<<< HEAD
=======
        $query->andFilterWhere(['like', 'ro.tanggal_pengajuan', $this->tanggalPengajuan]);
        $query->andFilterWhere(['like', 'ro.tanggal_penyetujuan', $this->tanggalPenyetujuan]);
>>>>>>> ea7b4f3fd30ed6d68a4074146bced5dd29909ea0
        $query->andFilterWhere(['like', 'ro.no_ro', $this->noRo]);

        return $dataProvider;
    }
}
