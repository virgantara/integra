<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PerusahaanSubSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cabang';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="perusahaan-sub-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cabang', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
<<<<<<< HEAD
=======
            'kode',
>>>>>>> ea7b4f3fd30ed6d68a4074146bced5dd29909ea0
            'nama',
            'namaPerusahaan',
         
            'created',

            [
<<<<<<< HEAD
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view' => function ($model) {
                        return \Yii::$app->user->can('admin');
                    },
                    'update' => function ($model) {
                        return \Yii::$app->user->can('admin');
                    },
                    'delete' => function ($model) {
                        return \Yii::$app->user->can('admin');
=======

                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view' => function ($model) {
                        return \Yii::$app->user->can('admin') || \Yii::$app->user->can('gudang') || \Yii::$app->user->can('operatorAdmin');
                    },
                    'update' => function ($model) {
                        return \Yii::$app->user->can('admin') || \Yii::$app->user->can('gudang') || \Yii::$app->user->can('operatorAdmin');
                    },
                    'delete' => function ($model) {
                        return \Yii::$app->user->can('admin') || \Yii::$app->user->can('gudang') || \Yii::$app->user->can('operatorAdmin');
>>>>>>> ea7b4f3fd30ed6d68a4074146bced5dd29909ea0
                    },
                ]
            ],
        ],
    ]); ?>
</div>
